#!/usr/bin/ruby

require 'optparse'
require 'fileutils'
require 'pathname'

$package = nil
$chroot = nil
$uchroot = nil
$chroots = []
$tests_failed = []
$tests_ok = []

Dir::chdir('/')

def create_chroot(chroot = $chroot)
  chr = `schroot -c #{chroot} -b 2>&1`.chomp
  if $?.exitstatus != 0
    puts "chroot creation FAILED!\n#{chr}"
    $tests_failed << 'create-chroot'
    exit(0)
  else
    $chroots << chr
    return chr
  end
  system("schroot -c #{chr} -r -- cp /proc/mounts /etc/mtab 2>&1")
end

def form(s)
  return s.split(/\n/).map { |l| '   ' + l }.join("\n")
end

opts = OptionParser::new do |opts|
  opts.banner = "Usage: ./instest [options]"
  opts.separator ""
  opts.separator "Options:"
  opts.on("-p", "--package PACKAGE", "") { |p| $package = p }
  opts.on("-c", "--chroot CHROOT", "") { |c| $chroot = c }
  opts.on("-u", "--upgrade-chroot CHROOT", "Specify a chroot to upgrade from (lenny chroot, usually)") { |c| $uchroot = c }
end
opts.parse!(ARGV)

# cleanup
Dir::chdir('/')
ENV['http_proxy']=''

$tstart = Time::now
at_exit do
  $chroots.each do |chr|
    STDOUT.flush
    system("schroot -c #{chr} -e")
    if $?.exitstatus != 0
      puts "Cleanup of chroot #{chr} failed. Running processes:"
      loc = `schroot -c #{chr} -i | grep "Mount Location" | awk '{print $3}'`.chomp
      loc = Pathname::new(loc).realpath
      system("lsof +D #{loc}")
      pids = `lsof -t +D #{loc}`.split.each do |pid|
        if Pathname::new(`readlink /proc/#{pid}/root`.chomp).realpath == loc
          system("kill -9 #{pid}")
        else
          puts "Skipping #{pid}: not correct root."
        end
      end
      system("schroot -c #{chr} -e")
      if $?.exitstatus != 0
        puts "Cleanup of chroot still failed. Stopping there."
        STDOUT.flush
        sleep 86400
      end
    end
  end
  realtime = Time::now - $tstart
  if $tests_failed.length == 0
    puts "-- Result: OK"
  else
    puts "-- Result: FAILED"
  end
  puts "-- Total time: #{realtime}s"
  puts "-- Tests OK: #{$tests_ok.join(' ')}"
  puts "-- Tests Failed: #{$tests_failed.join(' ')}"

  Dir::chdir("/")

  if $tests_failed.empty?
    exit(0)
  else
    exit(1)
  end
end

# start!
puts "IT-Header: #{$package} #{$chroot} / #{$tstart}"

# first test. install b-deps first, then package.
chr = create_chroot
$defaultinst = false
print "-- Checking if the package is already installed: "
s = `schroot -c #{chr} -r -- apt-cache policy #{$package} 2>&1`.chomp
if s =~ /Installed: \(none\)/
  puts "NO"
else
  puts "YES"
  $defaultinst = true
end
print "-- Finding version: #{$package} "
s = `schroot -c #{chr} -r -- apt-cache show --no-all-versions #{$package} 2>&1`.chomp
if $?.exitstatus != 0
  puts "FAILED\n#{form(s)}"
  $tests_failed << 'find-version'
  exit(0)
end
unstable_version = s.split(/\n/).grep(/^Version: /)[0].split(' ')[1]
puts unstable_version
STDOUT.flush

if $uchroot.nil?
  print "-- Finding depends and recommends: "
  s = `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -s install #{$package} 2>&1`.chomp
  if $?.exitstatus != 0
    puts "FAILED\n#{form(s)}"
    $tests_failed << 'find-deps'
    exit(0)
  end
  pkgs = (s.split(/\n/).grep(/^Inst /).map { |e| e.split(' ')[1] } - [$package])
  if pkgs != []
    puts pkgs.join(' ')
    print "-- Installing depends and recommends: "
    s = `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y install #{pkgs.join(' ')} 2>&1`
    if $?.exitstatus != 0
      puts "FAILED\n#{form(s)}"
      $tests_failed << 'inst-deps'
      puts "-- Installing depends failed, but we are trying to install the package directly anyway since apt might be able to do better."
    else
      puts "OK\n#{form(s)}"
      $tests_ok << 'inst-deps'
    end
  else
    puts "-- No depends to install, skipping dependencies installation."
  end
  STDOUT.flush

  $use_workaround_db = false
  def test_install_package(chr, testname, pkgm, instrecs)
    if pkgm == :apt
      if instrecs
        cmd = "schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -o APT::Install-Recommends=true -y install #{$package} 2>&1"
      else
        cmd = "schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -o APT::Install-Recommends=false -y install #{$package} 2>&1"
      end
    elsif pkgm == :aptitude
      cmd = "schroot -c #{chr} -r -- aptitude -y install #{$package} 2>&1"
    end
    s = `#{cmd}`
    if $?.exitstatus == 0
      puts "OK\n#{form(s)}"
    else
      puts "FAILED\n#{form(s)}"
      if s =~ /Is the server running locally and accepting connections on Unix domain socket "\/var\/run\/postgresql\// or
        s =~ /psql: could not connect to server: No such file or directory/ or
        s =~ /connections on Unix domain socket "\/var\/run\/postgresql\// or
        s =~ /Can't connect to local MySQL server through socket/ or
        s =~ /warning: database package not installed\?/ or
        $use_workaround_db
        # postgresql hack
        puts "-- Failed, maybe because postgresql or mysql was not running. Let's try after starting them."
        file = File::new('/tmp/instest-pg-lock', 'w')
        file.flock(File::LOCK_EX)
        print "-- Installing postgresql and mysql, ignoring result: "
        s = `schroot -c #{chr} -r -- apt-get -y install postgresql mysql-server 2>&1`
        puts "\n#{form(s)}"
        print "-- Starting postgresql: "
        #system("schroot -c #{chr} -r -- sed -i \"s/#listen_addresses = 'localhost'/listen_addresses = ''/\" /etc/postgresql/8.4/main/postgresql.conf")
        s = `schroot -c #{chr} -r -- /etc/init.d/postgresql start 2>&1`
        if $?.exitstatus != 0
          puts "FAILED\n#{form(s)}"
          $tests_failed << testname
          $tests_failed << 'start-postgresql'
          exit(0)
        end
        puts "OK\n#{form(s)}"
        print "-- Starting mysql: "
        system("schroot -c #{chr} -r -- sed -i \"s/^bind-address.*/skip-networking/\" /etc/mysql/my.cnf")
        s = `schroot -c #{chr} -r -- /etc/init.d/mysql start 2>&1`
        if $?.exitstatus != 0
          puts "FAILED\n#{form(s)}"
          $tests_failed << testname
          $tests_failed << 'start-mysql'
          exit(0)
        end
        puts "OK\n#{form(s)}"

        print "-- Retrying to install package: "
        s = `#{cmd}`
        if $?.exitstatus != 0
          puts "FAILED\n#{form(s)}"
          $tests_failed << testname
          $tests_failed << 'inst-after-postgresql'
          print "-- Stopping postgresql anyway: "
          s = `schroot -c #{chr} -r -- /etc/init.d/postgresql stop 2>&1`
          if $?.exitstatus != 0
            puts "FAILED\n#{form(s)}"
            $tests_failed << 'stop-postgresql'
          else
            puts "OK\n#{form(s)}"
          end
          print "-- Stopping mysql anyway: "
          s = `schroot -c #{chr} -r -- /etc/init.d/mysql stop 2>&1`
          if $?.exitstatus != 0
            puts "FAILED\n#{form(s)}"
            $tests_failed << 'stop-mysql'
          else
            puts "OK\n#{form(s)}"
          end
          exit(0)
        end
        puts "OK\n#{form(s)}"
        $use_workaround_db = true
        print "-- Stopping postgresql: "
        s = `schroot -c #{chr} -r -- /etc/init.d/postgresql stop 2>&1`
        if $?.exitstatus != 0
          puts "FAILED\n#{form(s)}"
          $tests_failed << testname
          $tests_failed << 'stop-postgresql'
          exit(0)
        end
        file.flock(File::LOCK_UN)
        puts "OK\n#{form(s)}"
        print "-- Stopping mysql: "
        s = `schroot -c #{chr} -r -- /etc/init.d/mysql stop 2>&1`
        if $?.exitstatus != 0
          puts "FAILED\n#{form(s)}"
          $tests_failed << testname
          $tests_failed << 'stop-mysql'
          exit(0)
        end
        puts "OK\n#{form(s)}"
      else
        $tests_failed << testname
        exit(0)
      end
    end
    if testname == 'inst-after-deps' and $tests_failed.include?('inst-deps')
      # installing the deps failed for some reason, but installing the package
      # worked. it might be caused by suboptimal apt choices. ignoring the inst-deps
      # error.
      $tests_failed.delete('inst-deps')
    end
    $tests_ok << testname
  end

  print "-- Installing the package after its depends and recommends: "
  test_install_package(chr, 'inst-after-deps', :apt, true)
  STDOUT.flush

  dchr = create_chroot
  print "-- Installing the package together with its depends, without recommends: "
  test_install_package(dchr, 'inst-with-deps', :apt, false)
  STDOUT.flush

  print "-- Creating new chroot and installing aptitude: "
  achr = create_chroot
  s = `schroot -c #{achr} -r -- apt-get -o Debug::pkgProblemResolver=true -y install aptitude 2>&1`
  if $?.exitstatus != 0
    puts "FAILED while installing aptitude\n#{form(s)}"
    $tests_failed << 'inst-aptitude'
    exit(0)
  else
    puts "OK\n#{form(s)}"
  end
  print "-- Installing the package with aptitude: "
  test_install_package(achr, 'inst-aptitude', :aptitude, true)
  STDOUT.flush

  chr = create_chroot
  print "-- Installing the package together with its depends and recommends: "
  pkgs_before = `schroot -c #{chr} -r -- dpkg -l 2>&1`.split(/\n/).grep(/^ii /).map { |e| e.split(/\s+/)[1] }
  test_install_package(chr, 'inst-with-recs', :apt, true)
  STDOUT.flush

  if $defaultinst
    puts "-- Package in default install, not testing removal."
  else
    pkgs_after = `schroot -c #{chr} -r -- dpkg -l 2>&1`.split(/\n/).grep(/^ii /).map { |e| e.split(/\s+/)[1] }
    print "-- Removing the package: "
    s = `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y --force-yes remove #{$package} 2>&1`
    if $?.exitstatus != 0
      puts "FAILED\n#{form(s)}"
      $tests_failed << 'rm-pkg'
    else
      puts "OK\n#{form(s)}"
      $tests_ok << 'rm-pkg'
      print "-- Removing all dependencies: "
      # ignore problems with adduser and ucf at this point
      # also about install-info, for now (bug filed against apt)
      deps = (pkgs_after - pkgs_before - [$package, 'adduser', 'ucf', 'update-inetd', 'perl-modules', 'install-info'])
      s = `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y --force-yes remove #{deps.join(' ')} 2>&1`
      if $?.exitstatus != 0
        puts "FAILED\n#{form(s)}"
        $tests_failed << 'rm-deps'
      else
        puts "OK\n#{form(s)}"
        $tests_ok << 'rm-deps'
        print "-- Purging package: "
        s = `schroot -c #{chr} -r -- dpkg --purge #{$package} 2>&1`
        if $?.exitstatus != 0
          puts "FAILED\n#{form(s)}"
          $tests_failed << 'purge-pkg'
        else
          puts "OK\n#{form(s)}"
          $tests_ok << 'purge-pkg'
        end
      end
    end
  end
end

if $uchroot != nil
  puts "-- Now testing upgrade from #{$uchroot}."
  chr = create_chroot($uchroot)
  print "-- Finding version in #{$uchroot}: #{$package} "
  s = `schroot -c #{chr} -r -- apt-cache show #{$package} 2>&1`.chomp
  sver = s.split(/\n/).grep(/^Version: /)
  if sver == []
    puts "NOT FOUND"
  else
    sver = sver[0].split(' ')[1]
    puts sver
    print "-- Installing #{$package} in #{$uchroot}: "
    s = `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y install #{$package} 2>&1`
    if $?.exitstatus != 0
      puts "FAILED\n#{form(s)}"
      $tests_failed << 'inst-old'
    else
      puts "OK\n#{form(s)}"
      $tests_ok << 'inst-old'
      print "-- Upgrading distribution: "
      s = "## Updating /etc/apt/sources.list...\n"
      s += `schroot -c #{chr} -r -- sed -i 's/ wheezy / jessie /' /etc/apt/sources.list 2>&1`
      s += "## Running apt-get update...\n"
      s += `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true update 2>&1`
#      s += "## Running apt-get -y install apt...\n"
#      s += `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y install apt 2>&1`
      s += "## Running apt-get -y upgrade...\n"
      s += `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y upgrade 2>&1`
      s += "## Running apt-get -y dist-upgrade...\n"
      s += `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y dist-upgrade 2>&1`
      if $?.exitstatus != 0
        puts "FAILED\n#{form(s)}"
        $tests_failed << 'upgrade-old'
      else
        puts "OK\n#{form(s)}"
        $tests_ok << 'upgrade-old'
        print "-- Finding version of #{$package}: "
        l = `schroot -c #{chr} -r -- dpkg -l 2>&1`.split(/\n/).grep(/^ii\s+#{Regexp::escape($package)}\s/)
        if l.length == 1
          new_version = l[0].split(/\s+/)[2]
          puts new_version
        elsif l == []
          new_version = "UNINSTALLED"
          puts new_version
        else
          p l
          exit(0)
        end
        print "-- New version is unstable version: "
        if new_version == unstable_version
          puts "OK"
          $tests_ok << 'pkg-upgrade'
        else
          puts "FAILED (#{new_version} != #{unstable_version})"
          $tests_failed << 'pkg-upgrade'
          print "-- Packages that will be removed if installed: "
          s = `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -s install #{$package} 2>&1`.chomp
          if $?.exitstatus != 0
            puts "FAILED\n#{form(s)}"
            $tests_failed << 'find-rm-after-upgrade'
            exit(0)
          end
          pkgs = (s.split(/\n/).grep(/^Remv /).map { |e| e.split(' ')[1] })
          puts pkgs.join(' ')
          print "-- Trying to install new version anyway: "
          s = `schroot -c #{chr} -r -- apt-get -o Debug::pkgProblemResolver=true -y install #{$package} 2>&1`
          if $?.exitstatus != 0
            puts "FAILED\n#{form(s)}"
            tests_failed << 'inst-new-after-upgrade'
          else
            puts "OK\n#{form(s)}"
          end
        end
      end
    end
  end
end
