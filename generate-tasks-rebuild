#!/usr/bin/ruby -w

require 'open-uri'
require 'pp'
require 'json'
require 'optparse'

EXCLUDED_PKGS = %w{
debian-installer
linux-modules-di-i386-2.6
linux-kernel-di-i386-2.6
linux-modules-di-amd64-2.6
linux-kernel-di-amd64-2.6
openclipart
posixtestsuite
}

$time_upper_bound = nil
$time_lower_bound = nil
$noall = false
$modes = []
$chroot = 'unstable'
$id = nil
$include_pkgs = nil
$exclude_pkgs = nil
options = OptionParser::new do |opts|
  opts.banner = "Usage: ./generate-tasks-json [options]"
  opts.separator ""
  opts.separator "Options:"
  opts.on("", "--no-arch-all", "No arch: all packages") { $noall = true }
  opts.on("-m", "--mode MODE", "Special mode") { |m| $modes << m }
  opts.on("-l", "--time-lower TIME", "Drop tasks that took less than this time") { |t| $time_lower_bound = t.to_i }
  opts.on("-u", "--time-upper TIME", "Drop tasks that took more than this time") { |t| $time_upper_bound = t.to_i }
  opts.on("-c", "--chroot CHROOT", "Chroot to use (default: unstable)") { |t| $chroot = t }
  opts.on("-i", "--id TEXT", "Additional identifier for logfiles") { |t| $id = t }
  opts.on("", "--include FILE", "List of packages to include") { |t| $include_pkgs = IO::read(t).split }
  opts.on("", "--exclude FILE", "List of packages to exclude") { |t| $exclude_pkgs = IO::read(t).split }
end
options.parse!(ARGV)

esttime = Hash[File::readlines(File.join(File.dirname(__FILE__), 'buildtime.list')).map { |l| l.split }.map { |e| [ e[0], e[1].to_i ] } ]

pkgs = open('https://udd.debian.org/cgi-bin/sources_rebuild.cgi').readlines.
          select { |l| l !~ /excluded-by-P-A-S/ }.
          map { |l| l.split }.
          reject { |e| EXCLUDED_PKGS.include?(e[0]) }.
          reject { |e| $noall and e[2] == 'all' }

tasks = []
pkgs.each do |e|
  t = {}
  t['source'] = e[0]
  t['version'] = e[1]
  t['architecture'] = e[2]
  t['chroot'] = $chroot
  t['esttime'] = esttime[e[0]] || nil
  logversion = t['version'].gsub(/^[0-9]+:/,'')
  if $id
    t['logfile'] = "/tmp/#{t['source']}_#{logversion}_#{t['chroot']}_#{$id}.log"
  else
    t['logfile'] = "/tmp/#{t['source']}_#{logversion}_#{t['chroot']}.log"
  end
  if $time_upper_bound and not t['esttime'].nil?
    next if t['esttime'] >= $time_upper_bound
  end
  if $time_lower_bound
    next if t['esttime'].nil? or t['esttime'] < $time_lower_bound
  end
  next if $include_pkgs and not $include_pkgs.include?(t['source'])
  next if $exclude_pkgs and $exclude_pkgs.include?(t['source'])
  t['modes'] = $modes
  tasks << t
end

puts JSON::pretty_generate(tasks)
