Running an archive rebuild
==========================
(this file was README in http://anonscm.debian.org/gitweb/?p=collab-qa/cloud-scripts.git;a=summary )

Overview of the process
-----------------------
The process is:
1. Ask for spot instances (typically 50x m1.medium + 10x m2.xlarge), wait for them
2. Connect to master node (which is always running), and start rebuild from it

0. Local configuration
----------------------
in .bashrc:
export EC2_PRIVATE_KEY=~/.ec2/pk-5ZQPKTH4FX2YWIYSKAHTFBCR4QGTKNN3.pem
export EC2_CERT=~/.ec2/cert-5ZQPKTH4FX2YWIYSKAHTFBCR4QGTKNN3.pem
export EC2_URL=https://ec2.us-west-2.amazonaws.com
then try 'ec2-describe-instances'

Those commands can be needed to set up the environment:
export PATH=$PATH:~/.ec2/bin
export EC2_HOME=~/.ec2
export JAVA_HOME=/usr/lib/jvm/default-java


You need to ask someone for those files.
You need someone to add your SSH key to the master node.

1. Requesting spot instances
----------------------------
When doing a full archive rebuild, you typically want two kind of build instances:
- medium instances, for the very large number of rather small packages
- xlarge instances, for the packages that benefit from such fast instances

AMI=ami-b9f591d6
NB=50
TYPE=m4.4xlarge
PRICE=5
SNAP=snap-0231fc7b # see ec2-describe-images $AMI
ZONE=eu-central-1b

Ireland: (ne marche pas)
AMI=ami-f98a6b80
SNAP=snap-014dd65e82e9705b8
ZONE=eu-west-1b
export EC2_URL=https://ec2.eu-west-1.amazonaws.com


# request instances (do it locally)
ec2-request-spot-instances -b /dev/sda1=$SNAP:200:true:gp2 -p $PRICE -t $TYPE -n $NB -z $ZONE $AMI -g ec2-master
# or (if you want to use non-spot instances -- much more expensive):
ec2-run-instances -b /dev/sda1=$SNAP:200:true:gp2 -t $TYPE -n $NB -z $ZONE $AMI -g ec2-master --instance-initiated-shutdown-behavior terminate

# wait for instances (do it locally)
ec2-describe-instances |grep INST |grep running | grep spot | grep $TYPE | awk '{print $14}' > nodes.$TYPE ; wc -l nodes.$TYPE

wc should return $NB (here, 10 or 50). If not, wait that the instances started.

Spot instances typically take 5 minutes to come up.
Once done, copy nodes.* to master node (find it with ec2-describe-instances):
scp nodes.* ec2:cloud-scripts/

2. Starting rebuild from master node
------------------------------------
Connect to master node (on root account). The host name of the master node can be found with the command ec2-describe-instance.

cd cloud-scripts/ (this is a checkout of the git repo mentioned above)

## Generate a list of tasks. examples:
TYPE=m1.medium
./generate-tasks-rebuild -u 1800 > tasks-$TYPE.json # for medium
TYPE=m2.xlarge
./generate-tasks-rebuild -l 1800 -m parallel > tasks-$TYPE.json # for xlarge

# test instances (from master node)
clush -l root -bw $(nodeset -f < nodes.$TYPE) true

# wait for nodes to finish booting and updating the chroots (might take a long time if the last image update
# was a long time ago
# watch the output of:
clush -l root -bw $(nodeset -f < nodes.$TYPE) 'ps fx |grep update'
Or:
clush -l root -bw $(nodeset -f < nodes.$TYPE) 'pgrep -cf ./update'

## Start a rebuild
screen -d -m -S $TYPE ./masternode -t tasks-$TYPE.json -n nodes.$TYPE -s 3 -o log.$TYPE -d /tmp/logdir
(see ./masternode -h)

To show the activity of the rebuild:
tail -f log.$TYPE

This:
- copies the local 'process-task' script on each node
- start tasks on nodes according to the number of slots (-s)

When booting, instances update the 'unstable' chroot.
Logs are copied to /tmp/logs on the master node
Instances are automatically shut down when no tasks are remaining, but it's a good idea to look for still-running instances in case some tasks just hang.
Custom rebuilds are generally done by modifying 'process-task' and using '-m' to add 'modes'. grep for 'clang' and 'default-jdk7' for examples.

2.1 Custom rebuild:

For example, the following command generates the rebuild with clang:
./generate-tasks-rebuild --no-arch-all -m binary-only -m clang -i clang > tasks-$TYPE.clang.json
(see ./generate-tasks-rebuild -h)

screen -d -m -S $TYPE ./masternode -t tasks-$TYPE.clang.json -n nodes.$TYPE -s 3 -o log.clang.$TYPE


3. Check for running instances after your rebuild
-------------------------------------------------
It can happen that some instances are still running after your rebuild. Make sure to clean up, either
using ec2-describe-instances, or using the web interface.
Some spot instances might not have been satisfied, and might result in instances starting later.
Check for them using ec2-describe-spot-instances.

More stuff (not normally needed)
--------------------------------
# Starting master node
ec2-describe-instances
ec2-start-instances i-b8fefff1
ec2-describe-instances

# test instances (from master node)
clush -bw $(nodeset -f < nodes.$TYPE) true

# install ganglia on instances
./setup-ganglia -n nodes.$TYPE

# Retry failed builds
psql -t -A -h localhost -U guest -p 5441 udd -c " select source, id from bugs where affects_unstable and severity = 'serious' and submitter_email = 'lucas@lucas-nussbaum.net' and id in (select id from bugs_usertags where email='debian-qa@lists.debian.org' and tag='qa-ftbfs');" |sed 's/|/ /'> openbugs
cat ../openbugs |awk '{print $1}' > failedpkgs
./generate-tasks-rebuild --include failedpkgs > tasks.json
screen -d -m -S foo ./masternode -t tasks.json -n nodes.$TYPE -s 2 -o log.retry


# To kill all running instances (on the workstation)
ec2-terminate-instances $(ec2-describe-instances |grep INST |grep running | grep spot | grep $TYPE | awk '{print $2}')
 
# instest
./generate-tasks-instest -d wheezy > tasks

./create-instest-chroots
cd /cloud/chroots
clush -bw $(nodeset -f < ~/cloud-scripts/nodes.$TYPE) --dest /cloud/chroots --copy *instest*tgz
cd /root/cloud-scripts
clush -bw $(nodeset -f < ~/cloud-scripts/nodes.$TYPE) --dest /root/cloud-scripts/ --copy instest
